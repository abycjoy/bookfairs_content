#!/bin/bash

# Enable and Disable modules
echo "Running modules_enabled.txt and modules_disabled.txt"
#drush --nocolor en -y $(cat modules_enabled.txt)
drush --nocolor dis -y $(cat modules_disabled.txt)

# perform any db queries required by code changes to modules from git pull
echo "Running any required database database updates"
drush --nocolor updatedb --yes
echo ""

# clear cache before attempting to revert the features.
# see https://drupal.org/node/1822278, https://drupal.org/node/602182
echo "Clearing the drupal cache before reverting the features"
drush --nocolor cc all
echo ""

# revert all features in Drupal to default code-based state.
echo "Reverting Drupal features to default code-based state"
drush --nocolor features-revert-all --yes
echo ""

# clear cache to seal the deal.
echo "Clearing the drupal cache again to finalize update"
drush --nocolor cc all
echo ""

